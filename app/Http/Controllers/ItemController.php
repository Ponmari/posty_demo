<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Items;


class ItemController extends Controller
{
    protected $globalService;
    public function __construct(GlobalService $globalService)
    {
        $this->items                   = new \App\Models\Items;
       }

    //list_Items
    public function listItems(Request $request)
    {
      $validator = Validator::make(
        $request->all(),
        [
          'length' => 'required',
          'start' => 'required',
          'draw' => 'required',
        ]
      );
      if ($validator->fails()) {
        $data['status'] = 0;
        $data['message'] = 'The requested resource could not be found.';
        $data['data'] = $validator->errors();
        return response()->json($data);
      }
  
      try {
        $data_array = $result = $item = $item_list = [];
        $item_count = 0;
        $item = Items::select('name','price','description')->whereNull('deleted_at');
  
        $item_count   = $item->count();
        $item_list    = $item->limit((int)$request->length)->offset($request->start)->orderBy("created_at", "DESC")->get();
  
        $data_array['user_list'] = $item_list;
        $data_array['draw'] = $request->draw;
        $data_array['recordsTotal'] = $item_count;
        $data_array['recordsFiltered'] = $item_count;
  
        $result['status']  = 1;
        $result['message'] = 'Items listed successfully';
        $result['data']    = $data_array;
      } catch (\Exception $ex) {
        $result['status']  = 0;
        $result['message'] = 'Oops!! Unable to process your request. Please check the data and try again.';
        $result['data']    = [];
      }
  
      return response()->json($result, 200);
    }

//add or update Items
public function addOrUpdateItems(Request $request)
{
    $result = array();
    try {
        $rules = array(
            'name' => 'required',
            'price' => 'required',
            );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $messages           = $validator->messages();
            $result['status']   = 0;
            $result['message']  = $messages;
            return response()->json($result);
        }
        $request_data   = $request->Input();
       
        $rep_status = array();
        $update_query = ["_id" => ""];
        if ($request->input("id")) {
            $update_query = ["_id" => $request->input("id")];
        }
        $requestData['name']  = (isset($requestData['name']) && !empty($requestData['name'])) ? $requestData['name'] : '';
        $requestData['price']      = (isset($requestData['price']) && !empty($requestData['price'])) ? $requestData['price'] : '';
       // $requestData['short_description']      = (isset($requestData['short_description']) && !empty($requestData['short_description'])) ? $requestData['short_description'] : '';
        $request_data['status'] = 1;
       $rep_status = $this->items->createOrUpdate($update_query, $request_data);
       if ($rep_status) {
            if ($request->input("id")) {
                $result = ["status" => 1, "message" => "Item Updated Successfully"];
            } else {
                $result = ["status" => 1, "message" => "Item Added Successfully"];
            }
        } else {
            $result = ["status" => 0, "message" => "Item cannot be added"];
        }
    } catch (\Exception $ex) {
        $result = ["status" => 0, "message" => $ex->getMessage()];
    }
    return response()->json($result, 200);
}

//delete Items
public function deleteItems($id)
{
    $result = array();
    try {
              if ($id) {
                     if (Items::find($id)->delete()) {
                $result = ["status" => 1, "message" => "Deleted Successfully"];
                return response()->json($result, 200);
            } else {
                $result = ["status" => 0, "message" => "Something wrong.try again!"];
                return response()->json($result, 200);
            }
        }
    } catch (\Exception $ex) {
        $result = ["status" => 0, "message" => "Oops!! Unable to process your request. Please check the data and try again."];
    }
    return response()->json($result, 200);
}

//view Items
public function viewItems($id)
{
    $result = [];
    try {
        $items = Items::select('name','price','description')->whereNull('deleted_at')->find($id);

        $result['status']   = 1;
        $result['message']  = "Items Listed Successfully";
        $result['data']['items']  = $items;
    } catch (\Exception $ex) {
        $result['status']  = 0;
        $result['message'] = 'Error:' . $ex->getMessage();
    }
    return response()->json($result, 200);
}

}

