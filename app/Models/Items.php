<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $collection = 'items';
    protected $fillable = ['name', 'price', 'description'];
    protected $attributes = [
        'deleted_at' => NULL,
          ]; 

 
    use HasFactory;
    public function createOrUpdate(array $attributes, array $values = array())
    {
        $instance = Items::firstOrNew($attributes);
        $instance->fill($values)->save();
       
        return $instance;
    }
}
