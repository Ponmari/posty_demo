<?php
namespace App;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 
Route::get('/list_items', 'ItemController@listItems');
Route::post('/add_or_update_items',	'ItemController@addOrUpdateItems');
Route::get('/view_items/{id}',			'ItemController@viewItems');
Route::get('/delete_items/{id}', 'ItemController@deleteItems');